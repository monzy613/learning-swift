//
//  ResourcesTableViewController.swift
//  LearningSwift
//
//  Created by Lorenzo Rheinicke on 2015/08/12.
//  Copyright (c) 2015 LCR Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ResourcesTableViewController: UITableViewController {

    var resources:[Resource] = []//[Resource]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //resources = []
        // Do any additional setup after loading the view, typically from a nib.
        
        //Get iOS Resources from MySQL RDS on AWS
        Alamofire.request(.GET, "http://goprovideos-env.elasticbeanstalk.com/getiosresources")
            .responseJSON { _, _, jsonObject, _ in
                //println("Response: \(jsonObject)")
                
                if let jsonObject:AnyObject = jsonObject {
                    
                    let json = JSON(jsonObject)
                    
                    for (key: String, subJson: JSON) in json {
                        
                        let id = subJson["id"].int
                        let title = subJson["title"].string
                        let description = ""
                        let resourceType = subJson["resourceType"].string
                        let url = subJson["url"].string
                        let resourceDescription = subJson["description"].string
                       
                        
                        let resource = Resource(id: id!, title: title!, description: resourceDescription!)
                        resource.resourceType = resourceType
                        resource.url = url
                        
                        self.resources.append(resource)
                    }
                    
                    //Reload Table
                    self.tableView.reloadData()
                
                }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        return resources.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("RightDetailCell", forIndexPath: indexPath) as! UITableViewCell
        
        let resource = self.resources[indexPath.row]
        //cell.te
        cell.textLabel?.text = resource.title
        cell.detailTextLabel?.text = resource.resourceType
        
        return cell
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detailVC = segue.destinationViewController as! ResourceDetailTableViewController
        let resource = self.resources[tableView.indexPathForSelectedRow()!.row]
        
        detailVC.resource = resource
    }
}
