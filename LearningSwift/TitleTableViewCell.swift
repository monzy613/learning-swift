//
//  TitleTableViewCell.swift
//  LearningSwift
//
//  Created by Lorenzo Rheinicke on 2015/08/12.
//  Copyright (c) 2015 LCR Technologies. All rights reserved.
//

import UIKit

class TitleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
